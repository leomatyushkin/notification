from django.contrib import admin

from .models import Broadcast, Client, Message, OperatorCode, Tag


@admin.register(Broadcast)
class BroadcastAdmin(admin.ModelAdmin):
    pass


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    pass


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    pass


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    pass


@admin.register(OperatorCode)
class OperatorCodeAdmin(admin.ModelAdmin):
    pass
