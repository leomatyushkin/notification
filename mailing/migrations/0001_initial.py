# Generated by Django 4.0.8 on 2022-11-19 19:49

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc
import timezone_field.fields
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Broadcast',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('date_time_start', models.DateTimeField(default=datetime.datetime(2022, 11, 19, 19, 49, 33, 170266, tzinfo=utc))),
                ('sms_text', models.CharField(max_length=160)),
                ('date_time_end', models.DateTimeField(default=datetime.datetime(2022, 11, 20, 19, 49, 33, 170929, tzinfo=utc))),
            ],
        ),
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('phone_number', models.CharField(max_length=11)),
                ('mobile_operator_code', models.CharField(max_length=3)),
                ('tz', timezone_field.fields.TimeZoneField(default='Europe/Moscow')),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=32, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('date_time_send', models.DateTimeField(default=datetime.datetime(2022, 11, 19, 19, 49, 33, 179564, tzinfo=utc))),
                ('departure_status', models.BooleanField()),
                ('broadcast_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mailing.broadcast')),
                ('client_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mailing.client')),
            ],
        ),
        migrations.AddField(
            model_name='client',
            name='tags',
            field=models.ManyToManyField(related_name='client_tags', to='mailing.tag'),
        ),
        migrations.AddField(
            model_name='broadcast',
            name='mobile_operator_code',
            field=models.ManyToManyField(db_column='mobile_operator_code', related_name='broadcast_mobile_operator_code', to='mailing.client'),
        ),
        migrations.AddField(
            model_name='broadcast',
            name='tag',
            field=models.ManyToManyField(related_name='broadcast_tag', to='mailing.tag'),
        ),
    ]
