import datetime
import uuid

from django.db import models
from django.utils import timezone
from timezone_field import TimeZoneField


class Broadcast(models.Model):
    """The model describes what will be sent and to whom"""

    # datetime of start of notification broadcasting
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    date_time_start = models.DateTimeField(default=timezone.now())

    # message text to be delivered to the client
    sms_text = models.CharField(max_length=160)

    # mobile operator codes to filter clients
    operator_code = models.ManyToManyField(to="OperatorCode")

    # tags to filter clients
    tag = models.ManyToManyField(to="Tag", related_name="broadcast_tag")

    # datetime of end of notification broadcasting
    # if for some reason we did not have time to
    # send all the messages, no messages should be
    # delivered to clients after this time
    date_time_end = models.DateTimeField(
        default=timezone.now() + datetime.timedelta(days=1)
    )

    def __str__(self):
        return f"{self.id}"


class Client(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    """The client that receives the messages"""
    phone_number = models.CharField(max_length=11)

    operator_code = models.CharField(blank=True, max_length=3)

    # optional tag
    tags = models.ManyToManyField(to="Tag", related_name="client_tags")

    tz = TimeZoneField(default="Europe/Moscow")

    @property
    def get_operator_code(self):
        code = self.phone_number[1:4]
        return code

    def save(self, *args, **kwargs):
        self.operator_code = self.get_operator_code
        super().save(*args, **kwargs)

        obj, created = OperatorCode.objects.get_or_create(code=self.operator_code)
        obj.save()

    def __str__(self):
        return f"{self.id}"


class Message(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    """Model of the current state of the message"""
    # datetime of message send act
    date_time_send = models.DateTimeField(default=timezone.now())

    # False for non-send
    departure_status = models.BooleanField()

    broadcast_id = models.ForeignKey("Broadcast", on_delete=models.CASCADE)

    client_id = models.ForeignKey("Client", on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.id}"


class Tag(models.Model):
    name = models.CharField(max_length=32, unique=True)

    def __str__(self):
        return f"{self.name}"


class OperatorCode(models.Model):
    code = models.CharField(max_length=3, unique=True)

    def __str__(self):
        return f"{self.code}"
