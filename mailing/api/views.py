from rest_framework import status
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, UpdateModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from mailing.models import Broadcast, Client, Message, OperatorCode, Tag

from .serializers import (
    BroadcastSerializer,
    ClientSerializer,
    MessageSerializer,
    OperatorCodeSerializer,
    TagSerializer,
)


class BroadcastViewSet(
    RetrieveModelMixin, ListModelMixin, UpdateModelMixin, GenericViewSet
):
    serializer_class = BroadcastSerializer
    queryset = Broadcast.objects.all()

    def get(self, request, pk=None):
        broadcasts = Broadcast.objects.all()
        serializer = BroadcastSerializer(broadcasts, many=True)
        return Response(serializer.data)

    def create(self, request, pk=None):
        data = request.data
        # serializer = BroadcastSerializer(data=data)

        new_broadcast = Broadcast.objects.create(
            date_time_start=data["date_time_start"],
            sms_text=data["sms_text"],
            date_time_end=data["date_time_end"],
        )

        for t in data["tag"]:
            tag_obj, created = Tag.objects.get_or_create(name=t)
            new_broadcast.tag.add(tag_obj)

        for oc in data["operator_code"]:
            operator_code_obj, created = OperatorCode.objects.get_or_create(code=oc)
            new_broadcast.operator_code.add(operator_code_obj)

        new_broadcast.save()

        return Response(status=status.HTTP_201_CREATED)

    def destroy(self, request, pk=None):
        try:
            broadcast = Broadcast.objects.get(id=pk)
            broadcast.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Broadcast.DoesNotExist:
            return Response(status=status.HTTP_404_BAD_REQUEST)


class ClientViewSet(
    RetrieveModelMixin, ListModelMixin, UpdateModelMixin, GenericViewSet
):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    def get(self, request, pk=None):
        clients = Client.objects.all()
        serializer = ClientSerializer(clients, many=True)
        return Response(serializer.data)

    def post(self, request, pk=None):
        serializer = ClientSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        try:
            client = Client.objects.get(id=pk)
            client.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Client.DoesNotExist:
            return Response(status=status.HTTP_404_BAD_REQUEST)


class MessageViewSet(
    RetrieveModelMixin, ListModelMixin, UpdateModelMixin, GenericViewSet
):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()


class TagViewSet(RetrieveModelMixin, ListModelMixin, UpdateModelMixin, GenericViewSet):
    serializer_class = TagSerializer
    queryset = Tag.objects.all()


class OperatorCodeViewSet(
    RetrieveModelMixin, ListModelMixin, UpdateModelMixin, GenericViewSet
):
    serializer_class = OperatorCodeSerializer
    queryset = OperatorCode.objects.all()
