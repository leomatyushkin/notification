import pytz
from rest_framework import serializers

from mailing.models import Broadcast, Client, Message, OperatorCode, Tag


class TagSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = Tag
        fields = ["id", "name"]

    def get_name(self, tag):
        return tag.name


class OperatorCodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = OperatorCode
        fields = ["id", "code"]


# class OperatorCodeListSerializer(serializers.RelatedField):
#     def to_representation(self, value):
#         return value.code


# class TagListSerializer(serializers.RelatedField):
#     def to_representation(self, value):
#         return value.name


class BroadcastSerializer(serializers.ModelSerializer):
    tag = TagSerializer(many=True)
    operator_code = OperatorCodeSerializer(many=True)

    class Meta:
        model = Broadcast
        fields = [
            "id",
            "date_time_start",
            "date_time_end",
            "sms_text",
            "operator_code",
            "tag",
        ]


class ClientSerializer(serializers.ModelSerializer):
    tz = serializers.ChoiceField(choices=pytz.all_timezones)

    class Meta:
        model = Client
        fields = ["id", "phone_number", "operator_code", "tags", "tz"]


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = [
            "id",
            "date_time_send",
            "departure_status",
            "broadcast_id",
            "client_id",
        ]
