from django.conf import settings
from django.urls import include, path
from rest_framework.routers import DefaultRouter, SimpleRouter

from mailing.api.views import (
    BroadcastViewSet,
    ClientViewSet,
    MessageViewSet,
    OperatorCodeViewSet,
    TagViewSet,
)
from notification.users.api.views import UserViewSet

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register(r"users", UserViewSet)
router.register(r"broadcasts", BroadcastViewSet)
router.register(r"clients", ClientViewSet)
router.register(r"messages", MessageViewSet)
router.register(r"tags", TagViewSet)
router.register(r"operatorcodes", OperatorCodeViewSet)


app_name = "api"
urlpatterns = [path("", include(router.urls))]
